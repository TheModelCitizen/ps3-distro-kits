# ps3-distro-kits

## PS3 Linux Distributor's Starter Kits

- Images of CDs distributed by Sony in the early days of the PS3 (when Sony still supported Linux on the platform).
- Excellent resource for anyone intersted in the notorious Cell Broadband Engine (the PS3's CPU) and/or PowerPC Linux in general.

## PS3 Linux & Cell/B.E. on the Web

- Visit my PS3 Linux & Cell/B.E. website at [ps3linux.net](http://ps3linux.net) - hosted on my own jailbroke PS3 Linux web server (no joke).
- My PS3 Linux YouTube channel: [www.youtube.com/channel/UCt1mQjAb5WNd9fGsDX-rc9A](https://www.youtube.com/channel/UCt1mQjAb5WNd9fGsDX-rc9A)
### The Model Citizen
